import Router from 'vue-router'
import home from './views/home.vue';
import map from './views/map.vue';

const routes = [
  {
    path: '/',
    component: home,
  },
  {
    path: '/map',
    component: map,
  },
]

export default new Router({
  routes
})
