import axios from "axios";
const url = "https://my-json-server.typicode.com/itayalony/VueDB";

export default {
    allCon: async () => {
        const rest = await axios.get(`${url}/convoies`);
        return await rest.data;
    },
    allBrief: async () => {
        const rest = await axios.get(`${url}/convoyBrief`);
        return await rest.data;
    },
    allProp: async () => {
        const rest = await axios.get(`${url}/convoyProp`);
        return await rest.data;
    },
    updateStatus: (convoyId, {isArrived, inMotion}) => {
        axios.patch(`${url}/convoies/${convoyId}`, {isArrived, inMotion});
    },
}