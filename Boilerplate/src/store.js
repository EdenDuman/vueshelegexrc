import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    selectedId: null,
    convoies: null,
    convoyBrief: null,
    convoyProp: null,
  },
  mutations: {
    updateConvoies(state, convoies) {
      state.convoies = convoies;
    },
    updateConvoyBrief(state, convoyBrief) {
      state.convoyBrief = convoyBrief;
    },
    updateConvoyProp(state, convoyProp) {
      state.convoyProp = convoyProp;
    },
    updateSelectedId(state, selectedId) {
      state.selectedId = selectedId;
    },
    updateStatos(state, {isArrived, inMotion}) {
      const selected = this.getters.selectedConvoy;
      selected.isArrived = isArrived;
      selected.inMotion = inMotion;
    }
  },
  actions: {
    updateConvoies({ commit }, convoies) {
      commit('updateConvoies', convoies);
    },
    updateConvoyBrief({ commit }, convoyBrief) {
      commit('updateConvoyBrief', convoyBrief);
    },
    updateConvoyProp({ commit }, convoyProp) {
      commit('updateConvoyProp', convoyProp);
    },
    updateSelectedId({ commit }, selectedId) {
      commit('updateSelectedId', selectedId);
    },
    updateStatus({ commit }, {isArrived, inMotion}) {
      commit('updateStatos', {isArrived, inMotion});
    }
  },
  getters: {
    convoies(state) {
      return state.convoies;
    },
    convoyBrief(state) {
      return state.convoyBrief;
    },
    convoyProp(state) {
      return state.convoyProp;
    },
    selectedId(state) {
      return state.selectedId;
    },
    selectedConvoy(state) {
      return state.convoies.filter((cuur) => cuur.id == state.selectedId)[0];
    },
    selectedConvoyId(state) {
      return state.selectedId;
    },
  },
})
