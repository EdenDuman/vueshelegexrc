import Vue from 'vue'
import vueRouter from 'vue-router'
import App from './App.vue'
import router from './router.js'

import store from './store'
require('@/assets/main.scss');

Vue.config.productionTip = false
Vue.use(vueRouter);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
